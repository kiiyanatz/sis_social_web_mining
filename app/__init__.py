from flask import Flask
from celery import Celery
from flask_socketio import SocketIO, emit

# App
APP = Flask(__name__)

APP.config.from_object('config')


# Turn the flask app into a socketio app
socketio = SocketIO(APP)

celery = Celery(APP.name, broker=APP.config['CELERY_BROKER_URL'])
celery.conf.update(APP.config)

from app import views, forms, models