from flask import render_template, request
from app import APP
from app import socketio

from app import celery
from sources.crawler_module import Crawler, MyCrawler
from sources.twitter_module import stream
# from sources.twitter_module import SisTwitter, stream


# @celery.task
# def start_crawl():
#     crawler = MyCrawler()
#     crawler.set_follow_mode(Crawler.F_SAME_HOST)
#     crawler.add_url_filter('\.(jpg|jpeg|gif|png|js|css|swf)$')
#
#     crawl_url = [
#         'http://www.the-star.co.ke/',
#         'http://www.standardmedia.co.ke/'
#     ]
#     for url in crawl_url:
#         crawler.crawl(url)
#
#
# @celery.task
# def start_search():
#     tweets = SisTwitter()
#     tweets.search('kiiya')


@celery.task
def start_stream():
    stream()


@APP.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')


@socketio.on('connect', namespace='/test')
def connect():
    print "Client connected"
    start_stream.delay()


@socketio.on('disconnect', namespace='/test')
def disconnect():
    print 'Client disconnected'
