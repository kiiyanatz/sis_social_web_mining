
$(document).ready(function(){
    //connect to the socket server.
    var socket = io.connect('http://' + document.domain + ':' + location.port + '/test');
    var tweets_received = [];
    var profile_received = [];

    //receive details from server
    socket.on('tweets', function(msg) {
        console.log("Received tweet" + msg.tweet.text);
        console.log("Profile image " + msg.tweet.user['profile_image_url']);
        //maintain a list of ten tweets
        if (tweets_received.length >= 10 && profile_received.length >=0){
            tweets_received.shift()
            profile_received.shift()
        }
        tweets_received.push(msg.tweet.text);
        profile_received.push(msg.tweet.user['profile_image_url']);
        tweet = '';
        profile_string = '';
        // img = '<img src=" '+ msg.tweet.user['profile_image_url'] +'" class="img-circle img-responsive">';
        for (var i = 0; i < tweets_received.length; i++){
            tweet = tweet + '<div class="row"><div class="col-md-2"> <img src=" '+ profile_received[i].toString() +'" class="img-circle img-responsive pull-right"> </div><div class="col-md-10 text-left"> <p>' + tweets_received[i].toString() + '</p></div></div>';
        }

        $('#tweet_text').html(tweet);
    });

});