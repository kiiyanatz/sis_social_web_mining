$(document).ready(function(){
    //connect to the socket server.
    var socket = io.connect('http://' + document.domain + ':' + location.port + '/test');
    var tweets_received = [];

    //receive details from server
    socket.on('tweets', function(msg) {
        console.log("Received tweet" + msg.tweet.text);
        //maintain a list of ten tweets
        if (tweets_received.length >= 10 && profile_received.length >=0){
            tweets_received.shift()
        }
        tweets_received.push(msg.tweet.text);
        tweet_string = '';

        for (var i = 0; i < tweets_received.length; i++){
            tweet_string = tweet_string + '<p>' + tweets_received[i].toString() + '</p>';
        }

        $('#log').html(tweet_string);
    });

});