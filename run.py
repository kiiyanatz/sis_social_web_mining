from app import APP, socketio

if __name__ == "__main__":
    socketio.run(APP)