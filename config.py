MONGOALCHEMY_DATABASE = 'tweets'

CELERY_BROKER_URL = 'redis://localhost:6379/0'

SECRET_KEY = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

# store status and results from tasks.
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
